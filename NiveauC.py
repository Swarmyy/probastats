"""
=================================================================================================================
Niveau C pour Probabilités Statistiques

Le fichier a traité doit être passé en argument 1
Et doit être un .csv, ou pouvoir être traité en tant que tel
Le nombre par lequel est multiplié la population doit être passé en argument 2
Et doit être un entier
Le nombre de jours que l'on passe à simuler doit être passé en argument 3
Et doit être un entier

Rappel de l'organisation du .csv:
pointName,pointPopulation,probasToGoToPoints
Avec les différentes probasToGoToPoints séparés par des ;
=================================================================================================================
"""

import csv
import os
import random
import numbers
import math
import numpy
import sys


def getCSV(file, delimiter):
    """
    Décrypte le CSV

    Parameters:
        file: le fichier à lire
        delimiter:le délimiteur entre les données

    Returns:
        Retourne la liste de listes composée de tout les éléments du CSV
    """
    thisdelimiter = delimiter
    if(os.path.exists(file)):
        if(os.path.isfile(file)):
            with open(file) as csvfile:
                reader = csv.reader(csvfile, delimiter=thisdelimiter)
                dataRows = []
                for row in reader:
                    dataRows.append(row)
                return dataRows
        else:
            pass
    else:
        pass


def proba(chance):
    if(random.uniform(0, 1) < chance):
        return True
    else:
        return False


def intervals(probas):
    # probas is a string
    rand = random.uniform(0, 1)
    for i in range(1, len(probas)):
        probas[i] += probas[i-1]
        if rand <= probas[i-1]:
            return i-1
    return i


def probabilite(dataRows, attributes, popTotal, nTries):
    for i in range(0, nTries):
        movPop = [0]*len(dataRows)
        for row in dataRows:
            for j in range(0, row[attributes.index("pointPopulation")]):
                probaGoToPoint = row[attributes.index(
                    "probasToGoToPoints")].split(';')
                probaGoToPoint = list(map(float, probaGoToPoint))
                l = intervals(probaGoToPoint)
                movPop[dataRows.index(row)] -= 1
                movPop[l] += 1
        print("Au jour %s" % i)
        for row in dataRows:
            row[attributes.index("pointPopulation")
                ] += movPop[dataRows.index(row)]
            print('Pourcentage au point %s: %s' % (row[attributes.index(
                "pointName")], row[attributes.index("pointPopulation")]/popTotal))
    return dataRows


"""
def stats(dataRows, attributes, nTries):
    listPoints = []
    for row in dataRows:
        listPoints.append(row[attributes.index("pointName")])
    allPathsint=[]
    allPaths=[]
    for point in listPoints:
        for i in range(0, nTries):
            path=path[:-1]
            allPathsint.append(float(statsPath(dataRows,attributes,path,listPoints)))
            allPaths.append(path)
    allPathsint.sort()
    for i in range(0,10):
        print("%s:%s"%(allPaths[i],allPathsint[i]))
        if(i>=nTries):
            break
"""


def stats(dataRows, attributes, n):
    M = numpy.array([list(map(float, row[attributes.index(
        "probasToGoToPoints")].split(';'))) for row in dataRows])
    pomn = []
    for row in dataRows:
        probaGoToPoint = row[attributes.index(
            "probasToGoToPoints")].split(';')
        probaGoToPoint = numpy.array(list(map(float, probaGoToPoint)))
        equation = numpy.matmul(
            probaGoToPoint, numpy.linalg.matrix_power(M, n))
        print('Pourcentage au point %s: %s' % (row[attributes.index(
            "pointName")], equation))
        pomn.append(equation)
    return pomn


def statsPath(dataRows, attributes, path, listPoints):
    # path=input("Chemin, séparé par des - : ")
    path = path.split('-')
    pathProbas = 1
    for i in range(0, len(path)-1):
        pathProbas *= float(dataRows[listPoints.index(path[i])][attributes.index(
            "probasToGoToPoints")].split(';')[listPoints.index(path[i+1])])
        i += 1
    return pathProbas


def main(delimiter):
    """
    Main
    """
    # Args
    file=sys.argv[1]
    popMultiplier=int(sys.argv[2])
    nTries=int(sys.argv[3])
    
    dataRows=getCSV(file,delimiter)
    attributes = dataRows.pop(0)
    popTotal = 0
    for row in dataRows:
        #NB: mind the CSVs who are not starting with a population at 1
        row[attributes.index("pointPopulation")] = int(
            row[attributes.index("pointPopulation")])*popMultiplier
        popTotal += row[attributes.index("pointPopulation")]
    dataRows = probabilite(dataRows, attributes, popTotal, nTries)
    matrixStat = stats(dataRows, attributes, nTries)
    for i in range(0, len(dataRows)):
        sum=0
        for j in range(0, len(dataRows)):
            difference = dataRows[i][attributes.index(
                "pointPopulation")]/popTotal-matrixStat[j].item(i)
            #print("Difference entre les 2 a %s vers %s : %s" % (dataRows[i][attributes.index(
            #    "pointName")], dataRows[j][attributes.index("pointName")], difference))
            sum+=difference/len(dataRows)
        #Ici on compare la moyenne des differences entre Expectations and Reality
        print("Difference entre les 2, Point %s : %s\uFF05"%(dataRows[i][attributes.index("pointName")],sum*100))


if __name__ == "__main__":
    main(',')
